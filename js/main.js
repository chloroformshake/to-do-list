const search = document.querySelector('#search')
const task = document.querySelector('#task')
const form = document.querySelector('form')
const ul = document.querySelector('ul')
const tickBoxes = document.querySelectorAll('.tickbox')
const removeTicked = document.querySelector('#remove-ticked')
const edit = document.querySelectorAll('#edit')
const remove = document.querySelectorAll('#remove')

const tickChangeEvent = (element) => {

    element.addEventListener('change', (e) => {

        const targetTask = e.target.parentElement.parentElement
        targetTask.classList.toggle('completed')
    })
}

const editTaskEvent = (element) => {

    element.children[0].addEventListener('click', (e) => {

        const targetTask = e.target.parentElement.parentElement

        if (!targetTask.classList.contains('completed')) {

            const targetText = e.target.parentElement.previousSibling
            const targetTickBox = e.target.parentElement.previousSibling.previousSibling.children[0]

            e.target.classList.toggle("save")

            if (e.target.className === "save") {

                e.target.innerHTML = "&#10004;";

                targetText.contentEditable = true
                targetText.style.backgroundColor = "silver"
                targetText.style.color = "black"
                targetTickBox.style.display = 'none'

            } else {

                e.target.innerHTML = "&#9997;"

                targetText.contentEditable = false
                targetText.style.backgroundColor = "inherit"
                targetText.style.color = "inherit"
                targetTickBox.style.display = 'inherit'
            }
        }
    })
}

const removeTaskEvent = (element) => {

    element.children[0].addEventListener('click', (e) => {
        if (!e.target.parentElement.parentElement.classList.contains('completed')) {
            ul.removeChild(e.target.parentElement.parentElement)
        }
    })
}

tickBoxes.forEach(children => tickChangeEvent(children))

edit.forEach(element => editTaskEvent(element))

remove.forEach(element => removeTaskEvent(element))

form.addEventListener('submit', (e) => {

    e.preventDefault()

    const addTask = task.value

    const li = document.createElement('li')

    const tickBox = document.createElement('div')
    tickBox.className = 'tickbox'

    const inputTickBox = document.createElement('input')
    inputTickBox.type = 'checkbox'
    tickBox.appendChild(inputTickBox)

    const text = document.createElement('text')
    text.textContent = addTask

    const edit = document.createElement('div')
    edit.className = 'actions'
    edit.id = 'edit'
    edit.innerHTML = '<span>&#9997;</span>'

    const remove = document.createElement('div')
    remove.className = 'actions'
    remove.id = 'remove'
    remove.innerHTML = '<span>&#10006;</span>'

    li.appendChild(tickBox)
    li.appendChild(text)
    li.appendChild(edit)
    li.appendChild(remove)

    ul.appendChild(li)

    tickChangeEvent(tickBox)
    editTaskEvent(edit)
    removeTaskEvent(remove)
})

removeTicked.addEventListener('click', () => {
    const liList = ul.childNodes

    let i = 0;
    while (true) {

        if (liList[i] === undefined) { break }
        if (liList[i].classList.contains("completed")) { ul.removeChild(liList[i]) }
        else { i += 1 }
    }
})

search.addEventListener('input', (e) => {

    for (i = 0; i < ul.children.length; i++) {

        const liText = ul.children[i].children[1].textContent

        if (e.target.value === "" || liText.toLowerCase().includes(e.target.value.toLowerCase())) {
            ul.children[i].style.display = 'flex'
        }
        else {
            ul.children[i].style.display = 'none'
        }
    }
})
